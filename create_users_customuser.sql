DROP TABLE IF EXISTS users_customuser;

CREATE TABLE users_customuser(
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "email" VARCHAR(50) NOT NULL UNIQUE,
    "password" VARCHAR(128) NOT NULL,
    "username" VARCHAR(128) UNIQUE,
    "last_login" DATETIME
);
