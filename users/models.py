# coding=utf-8
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

# TODO: Create substitute user model


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, username, **extra_fields):
        if not email:
            raise ValueError("Email field is required.")

        username = username or None
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            username=username,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, username, password, **extra_fields):
        return self._create_user(
                          email=email,
                          password=password,
                          username=username,
                          **extra_fields
        )


class CustomUser(AbstractBaseUser):
    username = models.CharField(unique=True, blank=True, max_length=128, null=True)
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = CustomUserManager()
