# coding=utf-8
from django.db.models import Q
from django.http import Http404

from .models import CustomUser

class EmailAuthBackend(object):
    # TODO: Enable users to login with email

    def authenticate(self, username=None, password=None):
    # Check the username/email and password and return a User.
        try:
            user = CustomUser.objects.get(
                Q(username__iexact=username) |
                Q(email__iexact=username)
            )
            if user.check_password(password):
                return user
            else:
                raise Http404("Wrong pass")
        except CustomUser.DoesNotExist:
            raise Http404("Wrong username/email")

    def get_user(self, pk=None):
        try:
            user = CustomUser.objects.get(pk=pk)
            return user
        except CustomUser.DoesNotExist:
            return None
    
